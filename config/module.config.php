<?php
return [
    'service_manager' => [
        'factories' => [
            'LForm\Config' => \LForm\Factory\LFormConfigFactory::class,
            'LForm\Service\SelectService' => \LForm\Factory\SelectServiceFactory::class
        ],
        'initializers' => [
            'TemplateInitializer' => 'LForm\Template\Initializer'
        ]
    ],
    'view_helpers' => [
        'invokables' => [
            'bootstrapTableRow' => 'ZfcDatagrid\Renderer\BootstrapTable\View\Helper\TableRow',
        ]
    ],
];
