<?php

namespace LFormTest\Service;

use LForm\Form\InstructionsFactory as FormFactory;
use LForm\Form\FormElementManager;
use LForm\Service\FormService;
use LForm\Service\InstructionsHttpClient;
use LForm\Template\Bootstrap;
use LForm\Form\View\HelperConfig;
use Zend\Http\Client\Adapter;
use Zend\Http\Response;
use Zend\ServiceManager\ServiceManager;
use Zend\View\HelperPluginManager;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class FormServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testGettingFormWithInvalidId() {
        $formService = new FormService($this->getClientMock(null, false));
        $result = $formService->getInstructions('invalid');
        $this->assertEquals(null, $result);
    }

    public function testGettingFormInstructions() {
        $instructions = ['test'=>'data'];
        $formService = new FormService($this->getClientMock($instructions));
        $result = $formService->getInstructions(213);
        $this->assertEquals($instructions, $result);
    }

    public function testUnableToCreateFormWithoutFactory()
    {
        $formService = new FormService($this->getClientMock([]));
        $this->setExpectedException('Exception');
        $formService->getForm(213);
    }

    public function testGettingFormWithId()
    {
        $instructions = ['name'=>'foo'];
        $elementManager = new FormElementManager();
        $elementManager->setServiceLocator(new ServiceManager());
        $factory = new FormFactory($elementManager);
        $formService = new FormService($this->getClientMock($instructions), $factory);
        /** @var \LForm\Form\Form $form */
        $form = $formService->getForm(213);
        $this->assertInstanceOf('\LForm\Form\Form', $form);
        $this->assertEquals('foo', $form->getName());
    }

    public function testFormRendering()
    {
        $etag = 'abc123';
        $instructions = ['name'=>'foo', 'fields' => [[
            'name' => 'editor',
            'type' => 'text',
        ]],
        'template' => [
            'ETag' => $etag,
            'default' => [
                'row' => '<div class="row {{name}}"><div class="col-xs-4">{{label}}</div><div class="col-xs-8">{{element}}{{errors}}{{warnings}}</div></div>',
                'row-no-label' => '<div class="row {{name}}"><div class="col-xs-8 col-xs-offset-4">{{element}}</div></div>',
                'errors' => '<ul class="error">{{error}}</ul>',
                'error' => '<li class="{{type}}">{{message}}</li>',
                'warnings' => '<ul class="warning">{{warning}}</ul>',
                'warning' => '<li class="{{type}}">{{message}}</li>',
            ],
            'field' => [

            ],
            'type' => [
                'checkbox' => ''
            ],
            'fieldset' => [
                'foo' => '<h1>Form</h1>{{lform-open}}{{editor}}{{lform-close}}'
            ]
        ]];
        $elementManager = new FormElementManager();
        $elementManager->setServiceLocator(new ServiceManager());
        $factory = new FormFactory($elementManager);
        $phpRenderer = new PhpRenderer();
        $phpRenderer->setHelperPluginManager(new HelperPluginManager(new HelperConfig()));
        $formService = new FormService($this->getClientMock($instructions), $factory, $phpRenderer);
        /** @var \Zend\View\Model\ViewModel $viewModel */
        $mapResolver = new TemplateMapResolver();
        $tplProvider = new Bootstrap('./data', $mapResolver);
        $viewModel = $formService->getViewModel(213, $tplProvider);
        $phpRenderer->setResolver($mapResolver);

        $this->assertInstanceOf('\Zend\View\Model\ViewModel', $viewModel);
        $tplProvider = $viewModel->getVariable('tplProvider');
        $this->assertInstanceOf('LForm\Template\TemplateProviderInterface', $tplProvider);
        $this->assertTrue($mapResolver->has('lform/' . $etag . '/fieldset_foo'));

        $html = $phpRenderer->render($viewModel);
        $this->assertContains('<h1>Form</h1>', $html);
        $this->assertContains('<div class="row editor"><div', $html);
        $this->assertContains('<div class="col-xs-8 col-xs-offset-4">', $html);
        $this->assertContains('<input type="text" name="editor"', $html);
        $this->assertNotContains('label', $html);
    }

    public function testGettingInvalidForm()
    {
        $elementManager = new FormElementManager();
        $elementManager->setServiceLocator(new ServiceManager());
        $factory = new FormFactory($elementManager);
        $formService = new FormService($this->getClientMock(null, false), $factory);
        $this->assertNull($formService->getForm(213));
    }

    /**
     * @param mixed $content
     * @param bool $isSuccess
     * @param string $method
     * @return callable
     */
    private function getClientMock($content = null, $isSuccess = true, $method = 'sendGet') {
        $resultMock = $this->getMockBuilder('LajiStoreClient\Http\Result')
            ->disableOriginalConstructor()
            ->getMock();
        $resultMock->expects($this->once())
            ->method('isSuccess')
            ->will($this->returnValue($isSuccess));
        if ($isSuccess) {
            $resultMock ->expects($this->once())
                ->method('getContent')
                ->will($this->returnValue($content));
        }

        /** @var \LajiStoreClient\Service\LajiStoreClient $clientMock */
        $clientMock = $this->getMockBuilder('LajiStoreClient\Service\LajiStoreClient')
            ->disableOriginalConstructor()
            ->getMock();
        $clientMock->expects($this->once())
            ->method($method)
            ->will($this->returnValue($resultMock));

        return new InstructionsHttpClient($clientMock);
    }
}