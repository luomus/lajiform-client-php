<?php
namespace Form\View\Helper;

use Zend\Form\Element;
use Zend\Form\View\HelperConfig;
use Zend\View\Renderer\PhpRenderer;
use LForm\Form\View\Helper\FormRow as FormRowHelper;

class FormRowTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var FormRowHelper
     */
    protected $helper;

    /**
     * @var PhpRenderer
     */
    protected $renderer;

    public function setUp()
    {
        $this->helper = new FormRowHelper();
        $this->renderer = new PhpRenderer();
        $helpers = $this->renderer->getHelperPluginManager();
        $config  = new HelperConfig();
        $config->configureServiceManager($helpers);

        $this->helper->setView($this->renderer);
    }

    public function testCanGenerateLabel()
    {
        $element = new Element('foo');
        $element->setLabel('The value for foo:');
        $markup = $this->helper->render($element);
        $this->assertContains('>The value for foo:<', $markup);
        $this->assertContains('<label', $markup);
        $this->assertContains('</label>', $markup);
    }

    public function testHasTemplate()
    {
        $this->assertInstanceOf('LForm\Template\TemplateProviderInterface', $this->helper->getTemplate());
    }

}