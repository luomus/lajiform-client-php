<?php

namespace LFormTest\Form;

use LForm\Form\InstructionsFactory as FormFactory;
use LForm\Form\FormElementManager;
use Zend\ServiceManager\ServiceManager;

class InstructionsFactoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var FormFactory
     */
    protected $factory;

    public function setUp()
    {
        $elementManager = new FormElementManager();
        $elementManager->setServiceLocator(new ServiceManager());
        $this->factory = new FormFactory($elementManager);
    }

    public function testCanCreateForms()
    {
        $form = $this->factory->createForm([
            'name'       => 'foo',
            'object'     => 'LFormTest\TestAsset\Model\Simple',
            'attributes' => [
                'method' => 'get',
            ],
        ]);
        $this->assertInstanceOf('LForm\Form\FormInterface', $form);
        $this->assertEquals('foo', $form->getName());
        $this->assertEquals('get', $form->getAttribute('method'));
        $this->assertEquals(new  \LFormTest\TestAsset\Model\Simple, $form->getObject());
    }


    public function testCanCreateFormsInputFilters()
    {
        $form = $this->factory->createForm([
            'name'         => 'haa',
            'fields' => [
                [
                    'name' => 'foo',
                    'type' => 'textarea',
                    'options' => [
                        'label' => 'foo label'
                    ],
                    'validators' => [
                        [
                            'name' => 'notEmpty'
                        ],
                        [
                            'name' => 'string_length',
                             'options' => [
                                'min' => 3,
                                'max' => 5,
                            ],
                        ]
                    ]
                ],
                [
                    'name' => 'bar',
                    'type' => 'text',
                    'options' => [
                        'label' => 'bar label'
                    ],
                    'required' => false,
                    'allow_empty' => true,
                    'filters'     => [
                        [
                            'name' => 'string_trim',
                        ]
                    ],
                ]
            ],
        ]);
        $this->assertInstanceOf('LForm\Form\FormInterface', $form);
        $this->assertTrue($form->has('foo'));
        $this->assertTrue($form->has('bar'));
        $this->assertInstanceOf('Zend\Form\Element\TextArea', $form->get('foo'));
        $this->assertInstanceOf('Zend\Form\Element\Text', $form->get('bar'));
        $filter = $form->getInputFilter();
        $this->assertInstanceOf('Zend\InputFilter\InputFilterInterface', $filter);
        $this->assertEquals(2, count($filter));
        foreach (['foo', 'bar'] as $name) {
            $input = $filter->get($name);
            switch ($name) {
                case 'foo':
                    $this->assertInstanceOf('Zend\InputFilter\Input', $input);
                    $this->assertTrue($input->isRequired());
                    $this->assertEquals(2, count($input->getValidatorChain()));
                    break;
                case 'bar':
                    $this->assertInstanceOf('Zend\InputFilter\Input', $input);
                    $this->assertFalse($input->isRequired());
                    $this->assertTrue($input->allowEmpty());
                    $this->assertEquals(1, count($input->getFilterChain()));
                    break;
                default:
                    $this->fail('Unexpected input named "' . $name . '" found in input filter');
            }
        }
    }

    public function testCanCreateFormsWarningInputFilters()
    {
        $form = $this->factory->createForm([
            'name'         => 'haa',
            'fields' => [
                [
                    'name' => 'foo',
                    'type' => 'text',
                    'required' => true,
                    'options' => [
                        'label' => 'foo label'
                    ],
                    'validators' => [
                        [
                            'name' => 'notEmpty'
                        ],
                        [
                            'name' => 'string_length',
                            'options' => [
                                'min' => 3,
                                'max' => 5,
                            ],
                        ]
                    ],
                    'warnings' => [
                        [
                            'name' => 'in_array',
                            'options' => [
                                'haystack' => [
                                    'foo',
                                    'bar'
                                ]
                            ],
                        ]
                    ]
                ]
            ],
        ]);
        $this->assertInstanceOf('LForm\Form\FormInterface', $form);
        $this->assertTrue($form->has('foo'));
        $filter = $form->getInputFilterWarning();
        $this->assertInstanceOf('Zend\InputFilter\InputFilterInterface', $filter);
        $this->assertEquals(1, count($filter));
        foreach (['foo'] as $name) {
            $input = $filter->get($name);
            switch ($name) {
                case 'foo':
                    $this->assertInstanceOf('Zend\InputFilter\Input', $input);
                    $this->assertFalse($input->isRequired());
                    $this->assertEquals(1, count($input->getValidatorChain()));
                    break;
                default:
                    $this->fail('Unexpected input named "' . $name . '" found in input filter');
            }
        }
        $filter = $form->getInputFilter();
        $this->assertInstanceOf('Zend\InputFilter\InputFilterInterface', $filter);
        $this->assertEquals(1, count($filter));
        foreach (['foo'] as $name) {
            $input = $filter->get($name);
            switch ($name) {
                case 'foo':
                    $this->assertInstanceOf('Zend\InputFilter\Input', $input);
                    $this->assertTrue($input->isRequired());
                    $this->assertEquals(2, count($input->getValidatorChain()));
                    break;
                default:
                    $this->fail('Unexpected input named "' . $name . '" found in input filter');
            }
        }
    }

    public function testFormWithCollection()
    {
        $form = $this->factory->createForm([
            'name'         => 'haa',
            'fields' => [
                [
                    'name' => 'collection',
                    'type' => 'collection',
                    'options' => [
                        'label' => 'foo label',
                        'allow_add' => true,
                        'allow_remove' => true,
                    ],
                    'fields' => [
                        [
                            'name' => 'foo',
                            'type' => 'textarea',
                            'required' => true,
                            'options' => [
                                'label' => 'foo label'
                            ],
                            'validators' => [
                                [
                                    'name' => 'notEmpty'
                                ],
                                [
                                    'name' => 'string_length',
                                    'options' => [
                                        'min' => 3,
                                        'max' => 5,
                                    ],
                                ]
                            ]
                        ],
                        [
                            'name' => 'bar',
                            'type' => 'text',
                            'options' => [
                                'label' => 'bar label'
                            ],
                            'required' => false,
                            'allow_empty' => true,
                            'filters'     => [
                                [
                                    'name' => 'string_trim',
                                ]
                            ],
                        ]
                    ]
                ],
                [
                    'name' => 'bar',
                    'type' => 'text',
                    'options' => [
                        'label' => 'bar label'
                    ],
                    'required' => false,
                    'allow_empty' => true,
                    'filters'     => [
                        [
                            'name' => 'string_trim',
                        ]
                    ],
                ]
            ],
        ]);
        $this->assertInstanceOf('LForm\Form\FormInterface', $form);
        $this->assertTrue($form->has('collection'));
        /** @var \Zend\Form\Element\Collection $collection */
        $collection = $form->get('collection');
        $this->assertInstanceOf('Zend\Form\Element\Collection', $collection);
        $fieldset = $collection->getTargetElement();
        $this->assertInstanceOf('Zend\Form\FieldSet', $fieldset);
        $this->assertEquals(2, count($fieldset));
        $this->assertTrue($fieldset->has('foo'));
        $this->assertTrue($fieldset->has('bar'));
        $filter = $form->getInputFilter();
        $this->assertInstanceOf('Zend\InputFilter\InputFilterInterface', $filter);
        $this->assertTrue($filter->has('collection'));
        /** @var \Zend\InputFilter\CollectionInputFilter $collectionFilter */
        $collectionFilter = $filter->get('collection');
        $this->assertInstanceOf('Zend\InputFilter\CollectionInputFilter', $collectionFilter);
        /** @var \Zend\InputFilter\CollectionInputFilter $collectionFilter */
        $filter = $collectionFilter->getInputFilter();
        foreach (['foo', 'bar'] as $name) {
            $input = $filter->get($name);
            switch ($name) {
                case 'foo':
                    $this->assertInstanceOf('Zend\InputFilter\Input', $input);
                    $this->assertTrue($input->isRequired());
                    $this->assertEquals(2, count($input->getValidatorChain()));
                    break;
                case 'bar':
                    $this->assertInstanceOf('Zend\InputFilter\Input', $input);
                    $this->assertFalse($input->isRequired());
                    $this->assertTrue($input->allowEmpty());
                    $this->assertEquals(1, count($input->getFilterChain()));
                    break;
                default:
                    $this->fail('Unexpected input named "' . $name . '" found in input filter');
            }
        }
    }

}