<?php

namespace LFormTest\Form;

use LForm\Form\Factory as FormFactory;
use LForm\Form\FormElementManager;
use Zend\ServiceManager\ServiceManager;

class FactoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var FormFactory
     */
    protected $factory;

    public function setUp()
    {
        $elementManager = new FormElementManager();
        $elementManager->setServiceLocator(new ServiceManager());
        $this->factory = new FormFactory($elementManager);
    }

    public function testCanCreateForms()
    {
        $form = $this->factory->createForm([
            'name'       => 'foo',
            'object'     => 'LFormTest\TestAsset\Model\Simple',
            'attributes' => [
                'method' => 'post',
            ],
        ]);
        $this->assertInstanceOf('LForm\Form\FormInterface', $form);
        $this->assertEquals('foo', $form->getName());
        $this->assertEquals('post', $form->getAttribute('method'));
        $this->assertEquals(new  \LFormTest\TestAsset\Model\Simple, $form->getObject());
    }


    public function testCanCreateFormsWithNamedInputFilters()
    {
        $form = $this->factory->createForm([
            'name'         => 'foo',
            'input_filter_warning' => 'LFormTest\TestAsset\InputFilter\Simple',
        ]);
        $this->assertInstanceOf('LForm\Form\FormInterface', $form);
        $filter = $form->getInputFilterWarning();
        $this->assertInstanceOf('LFormTest\TestAsset\InputFilter\Simple', $filter);
    }

    public function testCanCreateFormsWithInputFilterSpecifications()
    {
        $form = $this->factory->createForm([
            'name'         => 'foo',
            'input_filter_warning' => [
                'foo' => [
                    'name'       => 'foo',
                    'required'   => false,
                    'validators' => [
                        [
                            'name' => 'not_empty',
                        ],
                        [
                            'name' => 'string_length',
                            'options' => [
                                'min' => 3,
                                'max' => 5,
                            ],
                        ],
                    ],
                ],
                'bar' => [
                    'allow_empty' => true,
                    'filters'     => [
                        [
                            'name' => 'string_trim',
                        ]
                    ],
                ],
            ],
        ]);
        $this->assertInstanceOf('Zend\Form\FormInterface', $form);
        $filter = $form->getInputFilterWarning();
        $this->assertInstanceOf('Zend\InputFilter\InputFilterInterface', $filter);
        $this->assertEquals(2, count($filter));
        foreach (['foo', 'bar'] as $name) {
            $input = $filter->get($name);

            switch ($name) {
                case 'foo':
                    $this->assertInstanceOf('Zend\InputFilter\Input', $input);
                    $this->assertFalse($input->isRequired());
                    $this->assertEquals(2, count($input->getValidatorChain()));
                    break;
                case 'bar':
                    $this->assertInstanceOf('Zend\InputFilter\Input', $input);
                    $this->assertTrue($input->allowEmpty());
                    $this->assertEquals(1, count($input->getFilterChain()));
                    break;
                default:
                    $this->fail('Unexpected input named "' . $name . '" found in input filter');
            }
        }
    }

}