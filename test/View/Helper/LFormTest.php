<?php

namespace LFormTest\View\Helper;

use LForm\Template\Bootstrap;
use LForm\View\Helper\LForm;

class LFormTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var LForm
     */
    protected $helper;

    public function setUp()
    {
        $this->helper = new LForm();
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed.
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->helper);
    }

    public function testAwaysReturnTemplateProvider()
    {
        $helper = $this->helper;
        $this->assertInstanceOf('LForm\Template\TemplateProviderInterface', $helper::getTemplateProvider());

        $bootstrapTpl = new Bootstrap();
        $helper::setTemplateProvider($bootstrapTpl);

        $this->assertInstanceOf('LForm\Template\Bootstrap', $helper::getTemplateProvider());
    }


}