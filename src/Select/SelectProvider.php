<?php
namespace LForm\Select;

interface SelectProvider
{
    /**
     * Returns an array of options based on the the field name given
     *
     * @param $field
     * @return array
     */
    public function getSelectOptions($field); // : array;
}