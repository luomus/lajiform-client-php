<?php
namespace LForm\Select;


use Zend\Http\Client;
use Zend\Json\Json;

class HttpSelectClient extends Client implements SelectProvider
{
    protected $baseUrl;

    public function __construct($uri = null, $options = null)
    {
        $this->setBaseUrl($uri);
        parent::__construct($uri, $options);
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param mixed $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = (string) $baseUrl;
    }

    /**
     * Returns an array of options based on the the field name given
     *
     * @param $field
     * @return array
     */
    public function getSelectOptions($field)
    {
        if (is_null($this->baseUrl)) {
            return [];
        }
        $url = sprintf($this->baseUrl, $field);
        $this->setUri($url);
        $response = $this->send();
        if (!$response->isSuccess()) {
            return [];
        }
        return Json::decode($response->getContent(), Json::TYPE_ARRAY);
    }
}