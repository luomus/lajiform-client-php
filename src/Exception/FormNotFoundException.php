<?php
namespace LForm\Exception;

use DomainException;

class FormNotFoundException extends DomainException implements ExceptionInterface
{
}