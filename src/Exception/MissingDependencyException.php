<?php

namespace LForm\Exception;

class MissingDependencyException extends RuntimeException
{
}
