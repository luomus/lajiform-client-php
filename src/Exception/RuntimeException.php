<?php

namespace LForm\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
