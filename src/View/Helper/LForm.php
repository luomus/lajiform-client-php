<?php

namespace LForm\View\Helper;

use LForm\Form\View\Helper\FormRow;
use LForm\Template\Plain;
use LForm\Template\TemplateProviderInterface;
use Zend\Form\Element\Collection;
use Zend\Form\ElementInterface;
use Zend\Form\Fieldset;
use Zend\Form\View\Helper\AbstractHelper;
use Zend\Form\View\Helper\FormCollection;

class LForm extends AbstractHelper
{
    private static $templateProvider;

    protected $formRowHelper;
    protected $formCollectionHelper;

    public function __invoke(ElementInterface $element, $allRest = false)
    {
        return $this->renderElement($element);
    }

    protected function renderElement(ElementInterface $element) {
        if ($element instanceof Collection) {
            $collectionHelper = $this->getFormCollectionHelper();
            return $collectionHelper($element);
        } else if ($element instanceof Fieldset) {
            $partial = self::getTemplateProvider()->getTplName($element->getName());
            $vars = [
                'form' => $element,
            ];
            return $this->view->render($partial, $vars);
        }
        $rowHelper = $this->getFormRowHelper();
        return $rowHelper($element);

    }


    /**
     * Retrieve the FormElement helper
     *
     * @return FormRow
     */
    protected function getFormRowHelper()
    {
        if ($this->formRowHelper) {
            return $this->formRowHelper;
        }

        if (method_exists($this->view, 'plugin')) {
            $this->formRowHelper = $this->view->plugin('form_row');
        }

        if (!$this->formRowHelper instanceof FormRow) {
            $this->formRowHelper = new FormRow();
        }

        return $this->formRowHelper;
    }

    protected function getFormCollectionHelper() {
        if ($this->formCollectionHelper) {
            return $this->formCollectionHelper;
        }

        if (method_exists($this->view, 'plugin')) {
            $this->formCollectionHelper = $this->view->plugin('form_collection');
        }

        if (!$this->formCollectionHelper instanceof FormCollection) {
            $this->formCollectionHelper = new FormCollection();
        }

        return $this->formCollectionHelper;
    }

    /**
     * @return TemplateProviderInterface
     */
    public static function getTemplateProvider()
    {
        if (self::$templateProvider === null) {
            self::$templateProvider = new Plain();
        }
        return self::$templateProvider;
    }

    /**
     * @param TemplateProviderInterface $templateProvider
     */
    public static function setTemplateProvider(TemplateProviderInterface $templateProvider)
    {
        self::$templateProvider = $templateProvider;
    }

}