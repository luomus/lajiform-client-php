<?php

namespace Mapper;


class Instructions
{

    protected $instructions;

    public function __construct($instructions = null)
    {
        $this->instructions = $instructions;;
    }

    /**
     * @return null|array
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * @param array $instructions
     */
    public function setInstructions(array $instructions)
    {
        $this->instructions = $instructions;
    }


    public function getSpecs()
    {

    }

}