<?php

namespace LForm\Service;

use LajiStoreClient\Http\Result;
use LajiStoreClient\Service\LajiStoreClient;
use LForm\Form\Form;
use LForm\Form\FormInterface;
use LForm\Form\InstructionsFactory;
use LForm\Exception;
use LForm\Form\InstructionsProvider;
use LForm\Template\TemplateProviderInterface;
use LForm\View\Helper\LForm;
use Zend\Http\Client\Adapter;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Renderer\RendererInterface;

class FormService implements InstructionsProvider
{
    private $instructionProvider;
    private $formFactory;
    private $renderer;

    public function __construct(
        InstructionsProvider $instructionProvider,
        InstructionsFactory $factory = null,
        PhpRenderer $renderer = null
    )
    {
        $this->instructionProvider = $instructionProvider;
        $this->formFactory = $factory;
        $this->renderer = $renderer;
    }

    /**
     * @return InstructionsFactory
     */
    public function getFormFactory()
    {
        return $this->formFactory;
    }

    /**
     * @param InstructionsFactory $formFactory
     */
    public function setFormFactory(InstructionsFactory $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param $id
     * @return Form
     */
    public function getForm($id)
    {
        try {
            $form = $this->getFormFromInstructions($this->getInstructions($id));
        } catch(Exception\FormNotFoundException $e) {
            return null;
        }

        return $form;
    }

    /**
     * @param $id
     * @return array
     */
    public function getInstructions($id, $inSpec = false)
    {
        $instruction = $this->instructionProvider->getInstructions($id);
        if ($inSpec) {
            $instruction = $this->formFactory->convertToSpec($instruction);
        }
        return $instruction;
    }

    /**
     * @return RendererInterface
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * @param RendererInterface $renderer
     */
    public function setRenderer(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * Return view model with the form in it
     * @param $id
     * @param TemplateProviderInterface $tplProvider
     * @param FormInterface|null $form
     * @return ViewModel
     */
    public function getViewModel($id, TemplateProviderInterface $tplProvider, FormInterface $form = null) {
        return $this->prepareView($this->getInstructions($id), $tplProvider, $form);
    }

    /**
     * Return html for the form
     *
     * @param $id
     * @param TemplateProviderInterface $tplProvider
     * @param FormInterface|null $form
     * @return string
     */
    public function renderForm($id, TemplateProviderInterface $tplProvider, FormInterface $form = null)
    {
        if (!$this->renderer instanceof RendererInterface) {
            throw new Exception\MissingDependencyException('No renderer set for FormService');
        }
        $viewModel = $this->getViewModel($id, $tplProvider, $form);
        $this->renderer->setResolver($tplProvider->getTemplateMapResolver());
        return $this->renderer->render($viewModel);
    }

    private function prepareView($instructions, TemplateProviderInterface $tplProvider, FormInterface $form = null) {
        $form = $form === null ? $this->getFormFromInstructions($instructions) : $form;
        if ($form instanceof Form) {
            $form->prepare();
        }
        LForm::setTemplateProvider($tplProvider);
        $vm = [
            'form' => $form,
            'tplProvider' => $tplProvider
        ];
        if (isset($instructions['template'])) {
            $tplProvider->analyzeTemplateInstructions($instructions['template'], $form);
        }
        $model = new ViewModel($vm);
        $model->setTerminal(true);
        $model->setTemplate($tplProvider->getTplName($form->getName()));
        return $model;
    }

    /**
     * @param $instructions
     * @return \LForm\Form\FormInterface
     */
    private function getFormFromInstructions($instructions) {
        if (!$this->formFactory instanceof InstructionsFactory) {
            throw new Exception\MissingDependencyException('No form factory set for FormService');
        }
        if (!is_array($instructions)) {
            throw new Exception\FormNotFoundException('Could get form data');
        }
        return $this->formFactory->createForm($instructions);
    }
}