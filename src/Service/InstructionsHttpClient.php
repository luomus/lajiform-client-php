<?php

namespace LForm\Service;


use LajiStoreClient\Service\LajiStoreClient;
use LForm\Exception\FormNotFoundException;
use LForm\Form\InstructionsProvider;

class InstructionsHttpClient implements InstructionsProvider
{
    private $client;

    public function __construct(LajiStoreClient $client)
    {
        $this->client = $client;
    }

    public function getInstructions($id)
    {
        $result = $this->client->sendGet(LajiStoreClient::RESOURCE_FORMS, $id);
        if (!$result->isSuccess()) {
            return null;
        }
        return $result->getContent();
    }
}