<?php

namespace LForm\Service;


use LForm\Select\SelectProvider;
use Zend\Cache\Storage\StorageInterface;

class SelectService implements SelectProvider
{
    protected $cache;
    protected $selectProvider;

    public function __construct(StorageInterface $cache, SelectProvider $selectProvider)
    {
        $this->cache = $cache;
        $this->selectProvider = $selectProvider;
    }


    /**
     * Returns an array of options based on the the field name given
     *
     * @param $field
     * @return array
     */
    public function getSelectOptions($field)
    {
        $cache = $this->cache;
        $cacheKey = md5($field);
        if ($cache->hasItem($cacheKey )) {
            $select = $cache->getItem($cacheKey);
        } else {
            $select = $this->selectProvider->getSelectOptions($field);
            $cache->setItem($cacheKey, $select);
        }
        return $select;
    }
}