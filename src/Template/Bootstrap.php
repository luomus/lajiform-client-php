<?php

namespace LForm\Template;


class Bootstrap extends AbstractTemplate
{

    protected $tplInstructions = [
        self::DEFAULT_KEY => [
            'row' => '<label class="col-sm-4 control-label">{{label}}</label><div class="col-sm-8">{{element}}</div>',
            'row-no-label' => '<div class="col-sm-8 col-sm-push-4">{{element}}</div>',
            'row-wrap' => '<div class="form-group {{name}}">{{content}}</div>',
            'input-class' => 'form-control',
            'label-class' => 'control-label',
            'label-start' => '<label %s>',
            'label-end' => '</label>',
            'collection-wrap' => '<fieldset class="collection-metadata {{name}}" {{template}}{{attributes}}><div class="row"><div class="col-sm-8">{{label}}</div><div class="col-sm-4">{{collection-add}}</div></div><div class="collection-elements">{{content}}</div></fieldset>',
            'collection-label-wrap' => '<legend>{{label}}</legend>',
            'collection-content-wrap' => '<div class="collection-element"><div class="row"><div class="col-sm-12">{{collection-del}}</div></div><div class="row"><div class="col-sm-12">{{content}}</div></div></div>',
            'collection-del' => '<button class="collection-del btn btn-danger">delete</button>',
            'collection-add' => '<button class="collection-add btn btn-success pull-right">+</button>',
        ]
    ];

    public function tplType()
    {
        return 'bootstrap';
    }

    protected function getDefaultFieldsetTpl()
    {
        return realpath(__DIR__ . '/../../view/lform/bootstrap/default.phtml');
    }
}