<?php
namespace LForm\Template;


use LForm\Exception\InvalidArgumentException;
use Zend\Form\FormInterface;
use Zend\View\Resolver\TemplateMapResolver;

abstract class AbstractTemplate implements TemplateProviderInterface
{
    protected $tplInstructions = [];

    protected $map = [];

    protected $tplBase;
    protected $tplMapResolver;
    protected $defaultFieldsetTpl;
    protected $needsGeneration = true;
    protected $ETag;

    public function __construct($tplBase = '', TemplateMapResolver $tplMapResolver = null)
    {
        $this->tplBase = rtrim($tplBase, '/');
        $this->tplMapResolver = $tplMapResolver;
        if ($tplMapResolver !== null) {
            $tplMapResolver->merge([self::DEFAULT_TPL_NAME => $this->getDefaultFieldsetTpl()]);
        } else {
            $this->map[self::DEFAULT_TPL_NAME] = $this->getDefaultFieldsetTpl();
        }
    }

    public function getTemplateMapResolver()
    {
        if ($this->tplMapResolver === null) {
            $this->tplMapResolver = new TemplateMapResolver();
        }
        return $this->tplMapResolver;
    }

    abstract protected function getDefaultFieldsetTpl();

    public function getETag()
    {
        return $this->ETag;
    }

    public function analyzeTemplateInstructions($instructions, FormInterface $form)
    {
        if (!isset($instructions['ETag'])) {
            throw new \Exception('ETag is required for template');
        }
        $this->ETag = $instructions['ETag'];
        $this->preparePath();
        if (isset($instructions[self::FIELDSET_KEY])) {
            foreach($instructions[self::FIELDSET_KEY] as $name => $tpl) {
                $this->prepareView($name, self::FIELDSET_KEY, $tpl);
            }
            unset($instructions[self::FIELDSET_KEY]);
        }
        $this->tplMapResolver->merge($this->map);
        $this->tplInstructions = array_replace_recursive($this->tplInstructions, $instructions);
    }

    protected function preparePath()
    {
        $path = [$this->tplBase, self::VIEW_BASE, $this->ETag];
        $fullPath = '';
        $needsGeneration = true;
        while($level = array_shift($path)) {
            $fullPath .= $level;
            if (!is_dir($fullPath)) {
                mkdir($fullPath);
                $needsGeneration = true;
            }
            if (!is_writeable($fullPath)) {
                throw new \Exception('Template directory '. $fullPath . ' is not writable.');
            }
            $fullPath .= '/';
        }
        $this->needsGeneration = $needsGeneration;
    }

    protected function prepareView($fieldName, $prefix, $tpl)
    {
        $tplName = $this->getTplName($fieldName, $prefix, false);
        $tplFile = $this->getTplFile($tplName);
        if ($this->needsGeneration && !file_exists($tplFile)) {
            $this->generateView($tplFile, $tpl);
        }
        $this->map[$tplName] = $tplFile;
    }

    protected function generateView($tplFile, $tpl)
    {
        $tpl = str_replace('{{lform-open}}', '<?php echo $this->form()->openTag($this->form) ?>', $tpl);
        $tpl = str_replace('{{lform-close}}', '<?php echo $this->form()->closeTag($this->form) ?>', $tpl);
        $tpl = str_replace('{{lform-rest}}', '<?php echo $this->LForm($this->form, $this->tplProvider, true) ?>', $tpl);
        $tpl = preg_replace('/{{(\w+)}}/m','<?php echo $this->LForm($this->form->get(\'$1\'), $this->tplProvider) ?>', $tpl);
        file_put_contents($tplFile, $tpl);
    }

    public function getTplName($fieldName, $prefix = TemplateProviderInterface::FIELDSET_KEY, $userFallBack = true)
    {
        $tplName = self::VIEW_BASE . '/' . $this->ETag . '/' . $prefix . '_' . strtolower($fieldName);
        if ($userFallBack && !isset($this->map[$tplName])) {
            return self::DEFAULT_TPL_NAME;
        }
        return self::VIEW_BASE . '/' . $this->ETag . '/' . $prefix . '_' . strtolower($fieldName);
    }

    protected function getTplFile($tplName)
    {
        return sprintf($this->tplBase . '/%s.phtml', $tplName);
    }


    public function setRowTpl($tpl, $name = null)
    {
        $this->setTplValue(self::FIELD_KEY, $name, self::ROW, $tpl);
    }

    public function getRowTpl($name = null, $hasLabel = true)
    {
        return $this->getTplValue(self::FIELD_KEY, $name, self::ROW, '{{element}}', $hasLabel);
    }

    public function setCollectionLabelWrapperTpl($tpl, $name = null)
    {
        $this->setTplValue(self::FIELD_KEY, $name, self::COLLECTION_LABEL_WRAP, $tpl);
    }

    public function getCollectionLabelWrapperTpl($name = null)
    {
        return $this->getTplValue(self::FIELD_KEY, $name, self::COLLECTION_LABEL_WRAP, '{{label}}');
    }

    public function getCollectionWrapperTpl($name = null)
    {
        return $this->getTplValue(self::FIELD_KEY, $name, self::COLLECTION_WRAP, '{{content}}');
    }

    public function setCollectionWrapperTpl($tpl, $name = null)
    {
        $this->setTplValue(self::FIELD_KEY, $name, self::COLLECTION_WRAP, $tpl);
    }

    public function getCollectionContentWrapperTpl($name = null)
    {
        return $this->getTplValue(self::FIELD_KEY, $name, self::COLLECTION_CONTENT_WRAP, '{{content}}');
    }

    public function setCollectionContentWrapperTpl($tpl, $name = null)
    {
        $this->setTplValue(self::FIELD_KEY, $name, self::COLLECTION_CONTENT_WRAP, $tpl);
    }

    public function getCollectionAddTpl($name = null)
    {
        return $this->getTplValue(self::FIELD_KEY, $name, self::COLLECTION_ADD);
    }

    public function setCollectionAddTpl($tpl, $name = null)
    {
        $this->setTplValue(self::FIELD_KEY, $name, self::COLLECTION_ADD, $tpl);
    }

    public function getCollectionDelTpl($name = null)
    {
        return $this->getTplValue(self::FIELD_KEY, $name, self::COLLECTION_DEL);
    }

    public function setCollectionDelTpl($tpl, $name = null)
    {
        $this->setTplValue(self::FIELD_KEY, $name, self::COLLECTION_DEL, $tpl);
    }

    public function getRowWrapperTpl($name = null)
    {
        return $this->getTplValue(self::FIELD_KEY, $name, self::ROW_WRAP, '{{content}}');
    }

    public function setRowWrapperTpl($tpl, $name = null)
    {
        $this->setTplValue(self::FIELD_KEY, $name, self::ROW_WRAP, $tpl);
    }

    public function setElementClass($element, $class, $name = null)
    {
        $this->setTplValue(self::FIELD_KEY, $name, $element, $class);
    }

    public function getElementClass($element, $name = null)
    {
        return $this->getTplValue(self::FIELD_KEY, $name, $element, '');
    }

    public function getLabelTpl($place)
    {
        return $this->getTplValue(self::FIELD_KEY, null, $this->getLabelKey($place), '');
    }

    public function setLabelTpl($place, $tpl)
    {
        $this->setTplValue(self::FIELD_KEY, null, $this->getLabelKey($place), $tpl);
    }

    public function getTypeClass($type, $name = null)
    {
        return $this->getTplValue(self::TYPE_KEY, $name, $type . self::TYPE_CLASS_SUFFIX, '');
    }

    public function setTypeClass($type, $tpl, $name = null)
    {
        $this->setTplValue(self::TYPE_KEY, $name, $type . self::TYPE_CLASS_SUFFIX, $tpl);
    }

    public function getTypeTpl($type, $name = null)
    {
        return $this->getTplValue(self::TYPE_KEY, $name, $type, '{{element}}');
    }

    public function setTypeTpl($type, $tpl, $name = null)
    {
        $this->setTplValue(self::TYPE_KEY, $name, $type, $tpl);
    }

    protected function getLabelKey($place) {
        return $place === 'start' ? self::LABEL_START : self::LABEL_END;;
    }

    protected function getTplValue($elem, $name, $part, $notFound = '', $hasLabel = true) {
        if ($name !== null && isset($this->tplInstructions[$elem][$name])) {
            $result = $this->tplInstructions[$elem][$name];
            if (is_string($result) && isset($this->tplInstructions[$result][$part])) {
                return $this->tplInstructions[$result][$part];
            } elseif(isset($this->tplInstructions[$elem][$name][$part])) {
                return $this->tplInstructions[$elem][$name][$part];
            }
        }
        if (!$hasLabel) {
            $part .= self::NO_LABEL_SUFFIX;
        }
        if (isset($this->tplInstructions[self::DEFAULT_KEY][$part])) {
            return $this->tplInstructions[self::DEFAULT_KEY][$part];
        }
        return $notFound;
    }

    protected function setTplValue($elem, $name, $part, $value) {
        if ($name !== null) {
            $this->tplInstructions[$elem][$name][$part] = $value;
        }
        $this->tplInstructions[self::DEFAULT_KEY][$part] = $value;
    }

}