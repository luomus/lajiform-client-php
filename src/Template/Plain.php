<?php

namespace LForm\Template;


class Plain extends AbstractTemplate
{
    protected $tplInstructions = [
        self::DEFAULT_KEY => [
            self::ROW => '{{label-open}}{{label}}{{element}}{{label-close}}',
            self::ROW_NO_LABLE => '{{element}}'
        ]
    ];

    public function tplType()
    {
        return 'plain';
    }

    protected function getDefaultFieldsetTpl()
    {
        return realpath(__DIR__ . '/../../view/lform/plain/default.phtml');
    }
}