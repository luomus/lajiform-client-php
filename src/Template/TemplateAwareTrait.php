<?php

namespace LForm\Template;

use LForm\View\Helper\LForm;

trait TemplateAwareTrait
{
    protected $template;

    public function getTemplate() {
        if ($this->template == null) {
            $this->template = LForm::getTemplateProvider();
        }
        return $this->template;
    }

    public function setTemplate(TemplateProviderInterface $template) {
        $this->template = $template;
    }
}