<?php

namespace LForm\Template;


use LForm\View\Helper\LForm;
use Template\TemplateAwareInterface;
use Zend\ServiceManager\InitializerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Initializer implements InitializerInterface
{
    public function initialize($instance, ServiceLocatorInterface $serviceLocator)
    {
        if ($instance instanceof TemplateAwareInterface) {
            $instance->setTemplate(LForm::getTemplateProvider());
        }
    }
}