<?php

namespace LForm\Template;

interface TemplateAwareInterface
{
    /**
     * @return TemplateProviderInterface
     */
    public function getTemplate();

    public function setTemplate(TemplateProviderInterface $template);
}