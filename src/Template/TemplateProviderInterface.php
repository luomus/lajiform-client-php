<?php

namespace LForm\Template;


use Zend\Form\FormInterface;
use Zend\View\Resolver\TemplateMapResolver;

interface TemplateProviderInterface
{
    const VIEW_BASE = 'lform';

    const FIELDSET_KEY = 'fieldset';
    const FIELD_KEY = 'field';
    const DEFAULT_KEY = 'default';
    const TYPE_KEY = 'type';

    const NO_LABEL_SUFFIX = '-no-label';
    const TYPE_CLASS_SUFFIX = '-class';

    const ROW = 'row';
    const ROW_NO_LABLE = 'row-no-label';
    const ROW_WRAP = 'row-wrap';

    const COLLECTION_CONTENT_WRAP = 'collection-content-wrap';
    const COLLECTION_WRAP = 'collection-wrap';
    const COLLECTION_LABEL_WRAP = 'collection-label-wrap';

    const COLLECTION_ADD = 'collection-add';
    const COLLECTION_DEL = 'collection-del';

    const INPUT_CLASS = 'input-class';

    const LABEL_CLASS = 'label-class';
    const LABEL_START = 'label-start';
    const LABEL_END = 'label-end';

    const TYPE_BOOTSTRAP = 'bootstrap';
    const TYPE_PLAIN     = 'plain';

    const DEFAULT_TPL_NAME = 'lform/default/tpl';

    public function __construct($viewTplBase = '', TemplateMapResolver $tplMapResolver = null);

    public function getETag();

    public function getTplName($formName);

    public function tplType();

    public function analyzeTemplateInstructions($instructions, FormInterface $form);

    public function getCollectionLabelWrapperTpl($name = null);
    public function setCollectionLabelWrapperTpl($tpl, $name = null);

    public function getCollectionContentWrapperTpl($name = null);
    public function setCollectionContentWrapperTpl($tpl, $name = null);

    public function getCollectionWrapperTpl($name = null);
    public function setCollectionWrapperTpl($tpl, $name = null);

    public function getCollectionAddTpl($name = null);
    public function setCollectionAddTpl($tpl, $name = null);

    public function getCollectionDelTpl($name = null);
    public function setCollectionDelTpl($tpl, $name = null);

    public function setRowTpl($tpl, $name = null);
    public function getRowTpl($name = null, $hasLabel = true);

    public function setRowWrapperTpl($tpl, $name = null);
    public function getRowWrapperTpl($name = null);

    public function getLabelTpl($place);
    public function setLabelTpl($place, $tpl);

    public function getElementClass($element, $name = null);
    public function setElementClass($element, $class, $name = null);

    public function getTypeTpl($type, $name = null);
    public function setTypeTpl($type, $tpl, $name = null);

    public function getTypeClass($type, $name = null);
    public function setTypeClass($type, $tpl, $name = null);

    public function getTemplateMapResolver();
}