<?php
namespace LForm\Form;

use Zend\Form\Factory as FormFactory;
use Traversable;
use Zend\Form\Exception;
use Zend\Form\FormInterface;
use Zend\InputFilter\InputFilterInterface;

class Factory extends FormFactory
{
    protected $factory;

    public function createForm($spec)
    {
        if (!isset($spec['type'])) {
            $spec['type'] = 'LForm\Form\Form';
        }
        return parent::createForm($spec);
    }

    public function configureForm(FormInterface $form, $spec)
    {
        parent::configureForm($form, $spec);
        if (isset($spec['input_filter_warning'])) {
            $this->prepareAndInjectInputFilterWarning($spec['input_filter_warning'], $form, __METHOD__);
        }
        return $form;
    }

    /**
     * Prepare an input filter instance and inject in the provided form
     *
     * If the input filter specified is a string, assumes it is a class name,
     * and attempts to instantiate it. If the class does not exist, or does
     * not extend InputFilterInterface, an exception is raised.
     *
     * Otherwise, $spec is passed on to the attached InputFilter Factory
     * instance in order to create the input filter.
     *
     * @param  string|array|Traversable $spec
     * @param  FormInterface $form
     * @param  string $method
     * @return void
     * @throws Exception\DomainException for unknown InputFilter class or invalid InputFilter instance
     */
    protected function prepareAndInjectInputFilterWarning($spec, FormInterface $form, $method)
    {
        if ($spec instanceof InputFilterInterface) {
            $form->setInputFilterWarning($spec);
            return;
        }

        if (is_string($spec)) {
            if (!class_exists($spec)) {
                throw new Exception\DomainException(sprintf(
                    '%s expects string input filter names to be valid class names; received "%s"',
                    $method,
                    $spec
                ));
            }
            $filter = new $spec;
            if (!$filter instanceof InputFilterInterface) {
                throw new Exception\DomainException(sprintf(
                    '%s expects a valid implementation of Zend\InputFilter\InputFilterInterface; received "%s"',
                    $method,
                    $spec
                ));
            }
            $form->setInputFilterWarning($filter);
            return;
        }

        $factory = $this->getInputFilterFactory();
        $filter  = $factory->createInputFilter($spec);
        if (method_exists($filter, 'setFactory')) {
            $filter->setFactory($factory);
        }
        $form->setInputFilterWarning($filter);
    }
}