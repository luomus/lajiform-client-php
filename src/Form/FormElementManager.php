<?php

namespace LForm\Form;

use Zend\Form\FormElementManager as ZendFormElementManager;

class FormElementManager extends ZendFormElementManager
{
    /**
     * Default set of helpers
     *
     * @var array
     */
    protected $invokableClasses = [
        'button'        => 'Zend\Form\Element\Button',
        'captcha'       => 'Zend\Form\Element\Captcha',
        'checkbox'      => 'Zend\Form\Element\Checkbox',
        'collection'    => 'Zend\Form\Element\Collection',
        'color'         => 'Zend\Form\Element\Color',
        'csrf'          => 'Zend\Form\Element\Csrf',
        'date'          => 'Zend\Form\Element\Date',
        'dateselect'    => 'Zend\Form\Element\DateSelect',
        'datetime'      => 'Zend\Form\Element\DateTime',
        'datetimelocal' => 'Zend\Form\Element\DateTimeLocal',
        'datetimeselect' => 'Zend\Form\Element\DateTimeSelect',
        'element'       => 'Zend\Form\Element',
        'email'         => 'Zend\Form\Element\Email',
        'fieldset'      => 'Zend\Form\Fieldset',
        'file'          => 'Zend\Form\Element\File',
        'form'          => 'LForm\Form\Form',
        'hidden'        => 'Zend\Form\Element\Hidden',
        'image'         => 'Zend\Form\Element\Image',
        'month'         => 'Zend\Form\Element\Month',
        'monthselect'   => 'Zend\Form\Element\MonthSelect',
        'multicheckbox' => 'Zend\Form\Element\MultiCheckbox',
        'number'        => 'Zend\Form\Element\Number',
        'password'      => 'Zend\Form\Element\Password',
        'radio'         => 'Zend\Form\Element\Radio',
        'range'         => 'Zend\Form\Element\Range',
        'select'        => 'Zend\Form\Element\Select',
        'submit'        => 'Zend\Form\Element\Submit',
        'text'          => 'Zend\Form\Element\Text',
        'textarea'      => 'Zend\Form\Element\Textarea',
        'time'          => 'Zend\Form\Element\Time',
        'url'           => 'Zend\Form\Element\Url',
        'week'          => 'Zend\Form\Element\Week',
        'triplestore'   => 'LForm\Form\Element\Triplestore',
        'plaintext'     => 'LForm\Form\Element\PlainText',
    ];

}