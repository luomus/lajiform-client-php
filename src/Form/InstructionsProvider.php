<?php

namespace LForm\Form;


interface InstructionsProvider
{
    public function getInstructions($id);

}