<?php

namespace LForm\Form;

use LForm\Converter\ConverterFromInterface;
use LForm\Converter\SpecConverter;
use Zend\Form\Exception;

class InstructionsFactory extends Factory
{
    /** @var  ConverterFromInterface */
    protected $converter;

    public function __construct(FormElementManager $formElementManager)
    {
        $this->converter = new SpecConverter();
        parent::__construct($formElementManager);
    }

    public function createForm($instructions, $id = null)
    {
        if ($this->converter instanceof ConverterFromInterface) {
            return parent::createForm($this->converter->convertFrom($instructions, $id));
        }
        return parent::createForm($instructions);
    }

    /**
     * @return ConverterFromInterface
     */
    public function getConverter()
    {
        return $this->converter;
    }

    /**
     * @param ConverterFromInterface $converter
     */
    public function setConverter(ConverterFromInterface $converter)
    {
        $this->converter = $converter;
    }
}