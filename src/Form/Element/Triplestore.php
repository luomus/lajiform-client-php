<?php

namespace LForm\Form\Element;

use Traversable;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class DatabaseSelect
 * @package LForm\Form\Element
 */
class Triplestore extends Select implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /**
     * @var bool
     */
    protected $disableInArrayValidator = false;

    protected $field;

    /**
     *
     * Set options for an element. Accepted options are:
     * - label: label to associate with the element
     * - label_attributes: attributes to use when the label is rendered
     * - value_options: list of values and labels for the select options
     * _ empty_option: should an empty option be prepended to the options
     * _ field: name of the field you want to get from the database (matches the subject in the triplestore)
     *
     * @param array|Traversable $options
     * @param boolean $reverse
     * @return void|\Zend\Form\Element\Select|ElementInterface
     * @throws \Zend\Form\Exception\InvalidArgumentException
     */
    public function setOptions($options, $reverse = false)
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        } elseif (!is_array($options)) {
            throw new Exception\InvalidArgumentException(
                'The options parameter must be an array or a Traversable'
            );
        }

        if (isset($options['field'])) {
            $this->setField($options['field']);
        }
        if (!isset($options['value_options']) && !isset($this->options['value_options'])) {
            $options['value_options'] = $this->getValueList();
        }

        parent::setOptions($options);
    }

    protected function getValueList()
    {
        /** @var \LForm\Service\SelectService $selectService */
        $selectService = $this->getServiceLocator()->getServiceLocator()->get('LForm\Service\SelectService');
        return $selectService->getSelectOptions($this->field);
    }

    public function getField()
    {
        return $this->field;
    }

    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }
}
