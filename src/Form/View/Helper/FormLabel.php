<?php

namespace LForm\Form\View\Helper;


use LForm\Template\TemplateAwareInterface;
use LForm\Template\TemplateAwareTrait;
use LForm\Exception;
use LForm\Template\TemplateProviderInterface;
use Zend\Form\ElementInterface;
use Zend\Form\LabelAwareInterface;

class FormLabel extends \Zend\Form\View\Helper\FormLabel implements TemplateAwareInterface
{
    use TemplateAwareTrait;

    public function openTag($attributesOrElement = null)
    {
        $tpl = $this->getTemplate();
        $start = $tpl->getLabelTpl('start');
        $class = $tpl->getElementClass(TemplateProviderInterface::LABEL_CLASS);
        if (null === $attributesOrElement) {
            return sprintf($start, empty($class) ? '' : 'class="' . $class . '"');
        }

        if (is_array($attributesOrElement)) {
            $attributes = $this->createAttributesString($attributesOrElement);
            $this->addClassToAttr($attributes, $class);
            return sprintf($start, $attributes);
        }

        if (!$attributesOrElement instanceof ElementInterface) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s expects an array or Zend\Form\ElementInterface instance; received "%s"',
                __METHOD__,
                (is_object($attributesOrElement) ? get_class($attributesOrElement) : gettype($attributesOrElement))
            ));
        }

        $id = $this->getId($attributesOrElement);
        if (null === $id) {
            throw new Exception\DomainException(sprintf(
                '%s expects the Element provided to have either a name or an id present; neither found',
                __METHOD__
            ));
        }

        $labelAttributes = [];
        if ($attributesOrElement instanceof LabelAwareInterface) {
            $labelAttributes = $attributesOrElement->getLabelAttributes();
        }

        $attributes = ['for' => $id];

        if (!empty($labelAttributes)) {
            $attributes = array_merge($labelAttributes, $attributes);
        }
        $this->addClassToAttr($attributes, $class);

        $attributes = $this->createAttributesString($attributes);
        return sprintf($start, $attributes);
    }

    public function closeTag()
    {
        $this->getTemplate()->getLabelTpl('end');
    }

    protected function addClassToAttr(&$attr, $class) {
        if (empty($class)) {
            return;
        }
        if (!isset($attr['class'])) {
            $attr['class'] = $class;
            return;
        }
    }

}