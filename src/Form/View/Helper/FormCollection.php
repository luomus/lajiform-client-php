<?php

namespace LForm\Form\View\Helper;

use RuntimeException;
use LForm\Template\TemplateAwareInterface;
use LForm\Template\TemplateAwareTrait;
use Zend\Form\Element\Collection;
use Zend\Form\View\Helper\FormCollection as ZendFormCollection;
use Zend\Form\Element;
use Zend\Form\ElementInterface;
use Zend\Form\Element\Collection as CollectionElement;
use Zend\Form\FieldsetInterface;
use Zend\Form\LabelAwareInterface;
use Zend\View\Helper\HelperInterface;

class FormCollection extends ZendFormCollection implements TemplateAwareInterface
{
    use TemplateAwareTrait;

    protected $allow_del = false;
    protected $allow_add = false;

    /**
     * Render a collection by iterating through all fieldsets and elements
     *
     * @param  ElementInterface $element
     * @return string
     */
    public function render(ElementInterface $element)
    {
        $renderer = $this->getView();
        if (!method_exists($renderer, 'plugin')) {
            // Bail early if renderer is not pluggable
            return '';
        }

        $replace = [
            '{{template}}' => '',
            '{{label}}' => '',
            '{{content}}' => '',
            '{{attributes}}' => '',
            '{{collection-add}}' => '',
            '{{collection-del}}' => '',
        ];

        $markup           = '';
        $name             = $element->getOption('original-name');
        $elementHelper    = $this->getElementHelper();
        $fieldsetHelper   = $this->getFieldsetHelper();
        $allowAdd         = $this->allow_add;
        $allowDel         = $this->allow_del;
        $iterateElements  = true;

        $replace['{{name}}'] = $name;

        if ($element instanceof CollectionElement) {
            $allowAdd = $this->allow_add = $element->allowAdd();
            $allowDel = $this->allow_del = $element->allowRemove();
            if ($element->shouldCreateTemplate()) {
                $replace['{{template}}']  = ' data-collection-template="' . $this->renderTemplate($element) . '""';
                $replace['{{template}}'] .= ' data-collection-placeholder="' . $element->getTemplatePlaceholder() . '""';
            }
        } else if ($element instanceof FieldsetInterface) {
            $partial = $this->getTemplate()->getTplName($name);
            $vars = [
                'form' => $element
            ];
            $markup .= $this->view->render($partial, $vars);
            $iterateElements = false;
        }
        if ($iterateElements) {
            foreach ($element->getIterator() as $elementOrFieldset) {
                if ($elementOrFieldset instanceof FieldsetInterface) {
                    $markup .= $fieldsetHelper($elementOrFieldset, $this->shouldWrap());
                } elseif ($elementOrFieldset instanceof ElementInterface) {
                    $markup .= $elementHelper($elementOrFieldset);
                }
            }
        }
        $replace['{{content}}'] = $markup;

        // Every collection is wrapped by a fieldset if needed
        if ($this->shouldWrap) {
            $attributes = $element->getAttributes();
            unset($attributes['name']);
            $replace['{{attributes}}'] = count($attributes) ? ' ' . $this->createAttributesString($attributes) : '';

            $label = $element->getLabel();
            if (!empty($label)) {
                if (null !== ($translator = $this->getTranslator())) {
                    $label = $translator->translate(
                        $label,
                        $this->getTranslatorTextDomain()
                    );
                }

                if (! $element instanceof LabelAwareInterface || ! $element->getLabelOption('disable_html_escape')) {
                    $escapeHtmlHelper = $this->getEscapeHtmlHelper();
                    $label = $escapeHtmlHelper($label);
                }
                $replace['{{label}}'] = str_replace('{{label}}',$label, $this->getTemplate()->getCollectionLabelWrapperTpl($name));
            }
            if ($allowAdd) {
                $replace['{{collection-add}}'] = $this->getTemplate()->getCollectionAddTpl($name);
            }
            if ($allowDel) {
                $replace['{{collection-del}}'] = $this->getTemplate()->getCollectionDelTpl($name);
            }
            if ($element instanceof Collection) {
                $markup = str_replace(
                    array_keys($replace),
                    array_values($replace),
                    $this->getTemplate()->getCollectionWrapperTpl($name)
                );
            } else {
                $markup = str_replace(
                    array_keys($replace),
                    array_values($replace),
                    $this->getTemplate()->getCollectionContentWrapperTpl($name)
                );
            }

        }

        return $markup;
    }

    /**
     * Only render a template
     *
     * @param  CollectionElement $collection
     * @return string
     */
    public function renderTemplate(CollectionElement $collection)
    {
        $elementHelper          = $this->getElementHelper();
        $escapeHtmlAttribHelper = $this->getEscapeHtmlAttrHelper();
        $fieldsetHelper         = $this->getFieldsetHelper();

        $templateMarkup         = '';

        $elementOrFieldset = $collection->getTemplateElement();

        if ($elementOrFieldset instanceof FieldsetInterface) {
            $templateMarkup .= $fieldsetHelper($elementOrFieldset, $this->shouldWrap());
        } elseif ($elementOrFieldset instanceof ElementInterface) {
            $templateMarkup .= $elementHelper($elementOrFieldset);
        }

        return $escapeHtmlAttribHelper($templateMarkup);
    }

    /**
     * Retrieve the element helper.
     *
     * @return HelperInterface|callable
     * @throws RuntimeException
     */
    protected function getElementHelper()
    {
        if ($this->elementHelper) {
            return $this->elementHelper;
        }

        if (method_exists($this->view, 'plugin')) {
            $this->elementHelper = $this->view->plugin($this->getDefaultElementHelper());
        }

        if (!$this->elementHelper instanceof HelperInterface) {
            throw new RuntimeException('Invalid element helper set in FormCollection. The helper must be an instance of Zend\View\Helper\HelperInterface.');
        }

        return $this->elementHelper;
    }

}