<?php

namespace LForm\Form\View\Helper;

use LForm\Template\TemplateAwareInterface;
use LForm\Template\TemplateAwareTrait;
use LForm\Template\TemplateProviderInterface;
use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormElement as ZendFormElement;

class FormElement extends ZendFormElement implements TemplateAwareInterface
{
    use TemplateAwareTrait;

    public function render(ElementInterface $element)
    {
        $name = $element->getName();
        $type = $element->getAttribute('type');
        $tpl = $this->getTemplate();

        if (isset($type)) {
            $elementClass = $tpl->getTypeClass($type, $name);
        }
        $elementClass = empty($elementClass) ?
            $tpl->getElementClass(TemplateProviderInterface::INPUT_CLASS, $name) : $elementClass;
        $classes = $element->getAttribute('class');
        if ($elementClass !== '') {
            if ($classes) {
                if (strpos($classes, $elementClass) === false) {
                    $element->setAttribute('class', trim($classes . ' ' . $elementClass));
                }
            } else {
                $element->setAttribute('class', $elementClass);
            }
        }
        if ($type === null) {
            return str_replace('{{element}}', $element->getValue(), $tpl->getTypeTpl($type, $name));
        }
        return str_replace('{{element}}', parent::render($element), $tpl->getTypeTpl($type, $name));
    }

}