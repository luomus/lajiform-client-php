<?php

namespace LForm\Form;

use Zend\Filter\FilterInterface;
use Zend\Form\Element;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\FieldsetInterface;
use Zend\Form\Form as ZendForm;
use Zend\InputFilter\InputFilterInterface;
use Zend\Stdlib\ArrayUtils;

/**
 * Class Form is a form that has warning messages
 * @package LForm\Form
 */
class Form extends ZendForm implements FormInterface
{
    protected $inputFilterWarning;
    protected $warningMessages = array();

    /**
     * Returns all error messages
     *
     * @param null $elementName
     * @return array|\Traversable
     */
    public function getMessages($elementName = null)
    {
        if ($elementName === null) {
            $formMessages = parent::getMessages();
            $filterMessages = $this->getInputFilter()->getMessages();
            return array_replace_recursive($filterMessages, $formMessages);
        }
        return parent::getMessages($elementName);
    }

    /**
     * Set the input filter used by this form
     *
     * @param  InputFilterInterface $inputFilter
     * @return FormInterface
     */
    public function setInputFilterWarning(InputFilterInterface $inputFilter = null)
    {
        $this->inputFilterWarning = $inputFilter;
        return $this;
    }

    /**
     * Retrieve input warning filter used by this form
     *
     * @return null|InputFilterInterface
     */
    public function getInputFilterWarning()
    {
        return $this->inputFilterWarning;
    }

    /**
     * Set a hash of element names/messages to use when warning validation fails
     *
     * @param  array|\Traversable $messages
     * @return Element|ElementInterface|FieldsetInterface
     * @throws Exception\InvalidArgumentException
     */
    public function setWarningMessages($messages)
    {
        if (!is_array($messages) && !$messages instanceof \Traversable) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s expects an array or Traversable object of messages; received "%s"',
                __METHOD__,
                (is_object($messages) ? get_class($messages) : gettype($messages))
            ));
        }

        $msg = array();
        foreach ($messages as $key => $messageSet) {
            if ($this->has($key)) {
                $msg[$key] = $messageSet;
            }
        }
        $this->warningMessages = $msg;

        return $this;
    }

    /**
     * Get validation warning messages, if any
     *
     * Returns a hash of element names/messages for all elements failing
     * validation, or, if $elementName is provided, messages for that element
     * only.
     *
     * @param  null|string $elementName
     * @return array|\Traversable
     * @throws Exception\InvalidArgumentException
     */
    public function getWarningMessages($elementName = null)
    {
        if (null === $elementName) {
            return $this->warningMessages;
        }

        if (!$this->has($elementName)) {
            throw new Exception\InvalidArgumentException(sprintf(
                'Invalid element name "%s" provided to %s',
                $elementName,
                __METHOD__
            ));
        }

        if (in_array($elementName, $this->warningMessages)) {
            return $this->warningMessages[$elementName];
        }
        return array();

    }

    public function isValid()
    {
        $result = parent::isValid();
        $warningFilter = $this->getInputFilterWarning();
        if ($warningFilter === null) {
            return $result;
        }
        $warningFilter->setData($this->data);
        $validationGroup = $this->getValidationGroup() ?:InputFilterInterface::VALIDATE_ALL;
        $warningFilter->setValidationGroup($validationGroup);

        $warnings = $warningFilter->isValid();
        if (!$warnings) {
            $messages = $this->array_filter_recursive($warningFilter->getMessages());
            $this->setWarningMessages($messages);
        }

        return $result;
    }

    private function array_filter_recursive($input)
    {
        foreach ($input as &$value) {
            if (is_array($value)) {
                $value = $this->array_filter_recursive($value);
            }
        }
        return array_filter($input);
    }

}
