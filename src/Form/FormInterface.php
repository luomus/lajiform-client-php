<?php
namespace LForm\Form;

use Zend\Form\FormInterface as ZendFormInterface;
use Zend\InputFilter\InputFilterInterface;

interface FormInterface extends ZendFormInterface
{
    /**
     * Set input filter
     *
     * @param  InputFilterInterface $inputFilter
     * @return FormInterface
     */
    public function setInputFilterWarning(InputFilterInterface $inputFilter);

    /**
     * Retrieve warning input filter
     *
     * @return InputFilterInterface
     */
    public function getInputFilterWarning();

    /**
     * @param null|string $elementName if not null will return messages
     * @return array
     */
    public function getWarningMessages($elementName = null);

}