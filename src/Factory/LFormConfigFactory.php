<?php
namespace LForm\Factory;

use LForm\Service\SelectService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LFormConfigFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return SelectService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = [];
        if ($serviceLocator->has('config')) {
            $config = $serviceLocator->get('config');
        }
        $lfogmConfig = [];
        if (isset($config['lform'])) {
            $lfogmConfig = $config['lform'];
        }
        return $lfogmConfig;
    }
}