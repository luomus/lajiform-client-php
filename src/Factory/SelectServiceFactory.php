<?php
namespace LForm\Factory;

use LForm\Select\HttpSelectClient;
use LForm\Select\SelectProvider;
use LForm\Service\SelectService;
use Zend\Cache\Storage\Adapter\Memory;
use Zend\Cache\StorageFactory;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SelectServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return SelectService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $lformConfig = $serviceLocator->get('LForm\Config');
        $cache = $this->getCache($lformConfig);
        $selectProvider = $this->getSelectProvider($lformConfig, $serviceLocator);

        return new SelectService($cache, $selectProvider);
    }

    private function getSelectProvider($config, ServiceLocatorInterface $serviceLocator) {
        $provider = null;
        if (isset($config['select-provider'])) {
            $providerConfig = $config['select-provider'];
            if (isset($providerConfig['http'])) {
                $base = isset($providerConfig['http']['url']) ? $providerConfig['http']['url'] : null;
                $options = isset($providerConfig['http']['options']) ? $providerConfig['http']['options'] : null;
                $provider = new HttpSelectClient($base, $options);
            } elseif (isset($providerConfig['class']) && $serviceLocator->has($providerConfig['class'])) {
                $provider = $serviceLocator->get($providerConfig['class']);
            }
        }
        if (!$provider instanceof SelectProvider) {
            throw new \Exception('Could not initialize select provider');
        }
        return $provider;
    }

    private function getCache($config)
    {
        if (isset($config['cache'])) {
            return StorageFactory::factory($config['cache']);
        }
        return new Memory();
    }
}