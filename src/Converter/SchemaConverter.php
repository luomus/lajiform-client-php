<?php

namespace LForm\Converter;

use Zend\Form\Exception;

class SchemaConverter implements ConverterFromInterface
{
    protected $fieldTypeMap = [
        'text' => ['type' => 'string'],
        'textarea' => ['type' => 'string'],
        'radio' => ['type' => 'boolean'],
        'button' => ['type' => 'boolean'],
        'hidden' => ['type' => 'string'],
        'time' => ['type' => 'string', 'format' => 'time'],
        'date' => ['type' => 'string', 'format' => 'date'],
        'datetime' => ['type' => 'string', 'format' => 'date-time'],
        'datetimelocal' => ['type' => 'string', 'format' => 'date-time'],
        'select' => ['type' => 'string'],
        'checkbox' => ['type' => 'boolean'],
        'integer' => ['type' => 'integer'],
        'integer:positiveInteger' => ['type' => 'integer', 'exclusiveMinimum' => 0],
        'integer:nonNegativeInteger' => ['type' => 'integer', 'minimum' => 0],
        'number' => ['type' => 'number'],
        'submit' => ['type' => 'boolean'],
        'plainText' => ['type' => 'string'],
        'collection' => ['type' => 'array'],
        'fieldset' => ['type' => 'object'],
        'anyObject' => ['type' => 'object'],
        'keyValue' => ['type' => 'object', 'additionalProperties' => ['type' => 'string']],
    ];

    protected $special = [
        'MY.wgs84Geometry' => 'genericObject',
        'wgs84Geometry' => 'genericObject',
        'MY.geometry' => 'genericObject',
        'geometry' => 'genericObject'
    ];

    protected $uiTypes = [
        'textarea' => 'textarea',
    ];

    protected $doNotTransferKeys = [
        'fields',
        '@type',
        '@context'
    ];

    protected $transferAsIsFromOptions = [
        'uniqueItems',
        'minItems',
        'maxItems'
    ];


    /**
     * Converts the given data from raw lajistore form data to another
     * @param $instructions
     * @param $id
     * @return mixed
     */
    public function convertFrom($instructions, $id = null)
    {
        if ($id !== null && !isset($instructions['attributes']['id'])) {
            $instructions['attributes']['id'] = $id;
        }
        $schema = [];
        $uiSchema = [];
        $validators = [];
        $warnings = [];
        $excludeFromCopy = [];
        if (isset($instructions['fields'])) {
            $this->analyzeFields($instructions['fields'], $schema, $uiSchema, $validators, $warnings, $excludeFromCopy);
        }
        $result = array_diff_key($instructions, array_flip($this->doNotTransferKeys));
        $result['uiSchema'] = $this->clearEmpty($uiSchema);
        if (isset($instructions['uiSchema'])) {
            $result['uiSchema'] = array_replace_recursive($instructions['uiSchema'], $result['uiSchema']);
        }
        $result['uiSchema'] = (object) $result['uiSchema'];
        $result['schema'] = $schema;
        $result['validators'] = (object) $this->clearEmpty($validators);
        $result['warnings'] = (object) $this->clearEmpty($warnings);
        $result['excludeFromCopy'] = $excludeFromCopy;
        if (isset($instructions['translations'])) {
            $result['translations'] = $instructions['translations'];
        }
        if (isset($instructions['extra'])) {
            $result['uiSchemaContext'] = $this->parseExtra($instructions['extra']);
        }

        return $result;
    }

    private function clearEmpty($array) {
        foreach ($array as &$value)
        {
            if (is_array($value))
            {
                $value = $this->clearEmpty($value);
            }
        }
        return array_filter($array, function ($var){
            return ($var !== []);
        });
    }


    private function analyzeFields($fields, &$schema, &$uiSchema, &$validators, &$warnings, &$excludeFromCopy, $path = '$')
    {
        if (!isset($schema['type'])) {
            $schema['type'] = 'object';
        }
        $properties   = [];
        $required     = [];
        foreach($fields as $field) {
            $this->analyzeProperty($field, $properties, $uiSchema, $required, $validators, $warnings, $excludeFromCopy, $path);
        }
        $schema['properties'] = $properties;
        $schema['required']   = $required;
    }

    private function analyzeProperty($field, &$properties, &$uiSchema, &$required, &$validators, &$warnings, &$excludeFromCopy, $path) {
        if (!isset($field['type']) || !isset($field['name'])) {
            throw new Exception\InvalidElementException('Missing type specification: ' .  json_encode($field));
        }
        $type = $field['type'];
        $name = $field['name'];
        if (isset($field['required']) && $field['required']) {
            $required[] = $name;
        }
        if (isset($this->special[$name])) {
            $property = $this->special($this->special[$name], $field);
            $newType = 'special';
        } else if (isset($this->fieldTypeMap[$type])) {
            $property = $this->fieldTypeMap[$field['type']];
            $newType = $property['type'];
        } else {
            throw new Exception\InvalidElementException('Unrecognized type "' . $type . '" given');
        }
        if (isset($field['label'])) {
            $property['title'] = $field['label'];
        }
        if (isset($field['value'])) {
            $property['default'] = $field['value'];
        }
        if (isset($field['options'])) {
            if (isset($field['options']['default'])) {
                $property['default'] = $field['options']['default'];
            }
            if (isset($field['options']['excludeFromCopy']) && $field['options']['excludeFromCopy'] === true) {
                $excludeFromCopy[] = $path . '.' . $name;
                $property['excludeFromCopy'] = true;
            }
        }
        $uiProperty = [];
        switch($newType) {
            case 'object':
                $childUiSchema = [];
                $childSchema   = [];
                if (!isset($validators[$name])) {
                    $validators[$name] = [];
                    $warnings[$name] = [];
                }
                $validators[$name]['properties'] = [];
                $warnings[$name]['properties'] = [];
                if (isset($field['fields'])) {
                    $this->analyzeFields($field['fields'], $childSchema, $childUiSchema, $validators[$name]['properties'], $warnings[$name]['properties'], $excludeFromCopy, $path . '.' . $name);
                } else {
                    throw new Exception\InvalidElementException('Could not find fields for "' . $name . '"');
                }
                $property = array_replace($property, $childSchema);
                $uiProperty = $childUiSchema;
                break;
            case 'array':
                $childUiSchema = [];
                $childSchema   = [];
                $property['type'] = 'array';
                if (isset($field['fields'])) {
                    if (!isset($validators[$name])) {
                        $validators[$name] = [];
                        $warnings[$name] = [];
                    }
                    $validators[$name]['items'] = ['properties' => []];
                    $warnings[$name]['items'] = ['properties' => []];
                    $this->analyzeFields(
                        $field['fields'],
                        $childSchema,
                        $childUiSchema,
                        $validators[$name]['items']['properties'],
                        $warnings[$name]['items']['properties'],
                        $excludeFromCopy,
                        $path . '.' . $name . '[*]'
                    );
                } else if (isset($field['options']['target_element']['type']) &&
                    isset($this->fieldTypeMap[$field['options']['target_element']['type']])) {
                    $childSchema = $this->fieldTypeMap[$field['options']['target_element']['type']];
                    $subType = $childSchema['type'];
                    if (isset($field['options']['value_options']) ) {
                        $childSchema['enum'] = array_keys($field['options']['value_options']);
                        $childSchema['enumNames'] = array_values($field['options']['value_options']);
                        $property['uniqueItems'] = true;
                    }
                } else {
                    throw new Exception\InvalidElementException('Could not find fields for "' . $name . '"');
                }
                $property['items'] = $childSchema;
                $uiProperty['items'] = $childUiSchema;
                break;
            default:
                if ($type === 'select' && isset($field['options']['value_options']) ) {
                    $property['enum'] = array_keys($field['options']['value_options']);
                    $property['enumNames'] = array_values($field['options']['value_options']);
                }
                if (isset($this->uiTypes[$type])) {
                    $uiSchema[$name]['ui:widget'] = $this->uiTypes[$type];
                }
        }
        if (isset($field['ui'])) {
            $uiProperty = array_replace_recursive($uiProperty, $field['ui']);
        }
        if (isset($field['validators'])) {
            if (isset($validators[$name])) {
                $validators[$name] = array_replace_recursive($validators[$name], $field['validators']);
            } else {
                $validators[$name] = $field['validators'];
            }
        }
        if (isset($field['warnings'])) {
            if (isset($warnings[$name])) {
                $warnings[$name] = array_replace_recursive($warnings[$name], $field['warnings']);
            } else {
                $warnings[$name] = $field['warnings'];
            }
        }
        if (isset($field['options'])) {
            foreach ($this->transferAsIsFromOptions as $optionKey) {
                if (isset($field['options'][$optionKey])) {
                    $property[$optionKey] = $field['options'][$optionKey];
                }
            }
        }
        $properties[$name] = $property;
        $uiSchema[$name] = $uiProperty;
    }

    private function parseExtra($extra) {
        $result = [];
        foreach($extra as $field => $data) {
            if (isset($data['altParent'])) {
                $result[$field]['tree'] = $this->parseTree($data['altParent']);
            }
        }
        return $result;
    }

    private function parseTree($tree) {
        $links = [];
        $roots = [];
        foreach($tree as $elem => $parents) {
            $roots[$elem] = true;
            if (empty($parents)) {
                $links[$elem] = new \stdClass();
            }
            foreach($parents as $parent) {
                if (is_numeric($parent)) {
                    continue;
                }
                if (!isset($roots[$parent])) {
                    $roots[$parent] = true;
                }
                if (!isset($roots[$elem]) || $roots[$elem]) {
                    $roots[$elem] = false;
                }
                if (!isset($links[$parent])) {
                    $links[$parent] = ['children' => [], 'order' => []];
                } else if ($links[$parent] instanceof \stdClass) {
                    $links[$parent] = ['children' => [], 'order' => []];
                }
                if (!isset($links[$elem])) {
                    $links[$elem] = new \stdClass();
                }
                $links[$parent]['children'][$elem] = &$links[$elem];
                $links[$parent]['order'][] = $elem;
            }
        }
        $roots = array_filter($roots);
        foreach($roots as $root => $val) {
            $roots[$root] = $links[$root];
        }
        return ['children' => $roots, 'order' => array_keys($roots)];
    }

    private function special($type, $field) {
        switch($type) {
            case 'genericObject':
                return [
                    'type' => 'object',
                    'properties' => new \stdClass()
                ];
        }
    }

}