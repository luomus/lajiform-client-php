<?php
namespace LForm\Converter;


interface ConverterFromInterface
{
    /**
     * Converts the given data from raw lajistore form data to another
     * @param $instructions
     * @param $id
     * @return array in converted form
     */
    public function convertFrom($instructions, $id = null);

}