<?php

namespace LForm\Converter;

use Zend\Form\Exception;

class SpecConverter implements ConverterFromInterface
{
    protected $fieldTypeMap = [
        'text' => 'text',
        'textarea' => 'textarea',
        'radio' => 'radio',
        'button' => 'button',
        'hidden' => 'hidden',
        'datetime' => 'datetime',
        'datetimelocal' => 'datetimelocal',
        'triplestore' => 'triplestore',
        'checkbox' => 'boolean',
        'integer' => 'text',
        'select' => 'select',
        'submit' => 'submit',
        'plainText' => 'plaintext'
    ];

    protected $fieldSetTypeMap = [
        'collection' => 'collection',
        'fieldset' => 'fieldset',
    ];

    /**
     * Converts the given data from raw lajistore form data to another
     * @param $instructions
     * @param $id
     * @return mixed
     */
    public function convertFrom($instructions, $id = null)
    {
        if ($id !== null && !isset($instructions['attributes']['id'])) {
            $instructions['attributes']['id'] = $id;
        }
        $spec = $this->filterOut($instructions, 'fieldSet');
        if (isset($instructions['fields'])) {
            $this->analyzeFields($instructions['fields'], $spec);
        }
        if (isset($spec['input_filter_warning'])) {
            $json = json_encode($spec['input_filter_warning']);
            $json = str_replace('"input_filter_warning"','"input_filter"', $json);
            $spec['input_filter_warning'] = json_decode($json, true);
        }

        return $spec;
    }


    private function analyzeFields($fields, &$spec)
    {
        $elements = [];
        $fieldSet = [];
        $input_filter = [];
        $input_filter_warning = [];
        foreach($fields as $field) {
            if (!isset($field['type']) || !isset($field['name'])) {
                throw new Exception\InvalidElementException('Missing type specification');
            }
            $type = $field['type'];
            $name = $field['name'];
            if (isset($field['label'])) {
                $field['options']['label'] = $field['label'];
            }
            if (isset($this->fieldSetTypeMap[$type])) {
                $type = $this->fieldSetTypeMap[$type];
                $subSpec = $this->filterOut($field, 'fieldSet');
                $subSpec['type'] = 'fieldset';
                $subSpec['options']['original-name'] = $name;
                if ($type === 'collection') {
                    $this->addCollection($field, $subSpec, $name, $elements, $input_filter, $input_filter_warning);
                }
                continue;
            } else if (isset($this->fieldTypeMap[$type])) {
                $field['type'] = $this->fieldTypeMap[$field['type']];
                $fieldSpec = $this->filterOut($field);
                $fieldSpec['options']['original-name'] = $name;
                $elements[] = ['spec' => $fieldSpec];
            } else {
                throw new Exception\InvalidElementException('Unrecognized type "' . $type . '" given');
            }
            $inputFilterSpec = $this->filterOut($field, 'inputFilter');
            if (count($inputFilterSpec) > 1) {
                $input_filter[$name] = $inputFilterSpec;
            }
            if (isset($field['warnings']) || isset($field['requiredInWarning'])) {
                $input_filter_warning[$name] = [
                    'name' => $name,
                    'required' => isset($field['requiredInWarning']) ? $field['requiredInWarning'] : false,
                    'validators' => isset($field['warnings']) ? $field['warnings'] : []
                ];
            }
        }
        $spec['elements'] = $elements;
        $spec['fieldsets'] = $fieldSet;
        $spec['input_filter'] = $input_filter;
        $spec['input_filter_warning'] = $input_filter_warning;
    }

    /**
     * @param $field
     * @param $subSpec
     * @param $name
     * @param $elements
     */
    private function addCollection(&$field, &$subSpec, $name, &$elements, &$input_filter, &$input_filter_warning)
    {
        if (isset($field['fields'])) {
            $this->analyzeFields($field['fields'], $subSpec);
        }
        foreach (['input_filter', 'input_filter_warning'] as $subField) {
            if (!isset($subSpec[$subField])) {
                continue;
            }
            ${$subField}[$name] = [
                'name' => $name,
                'type' => 'collection',
                $subField => $subSpec[$subField]
            ];
            unset($subSpec[$subField]);
        }
        if (!isset($field['options'])) {
            $field['options'] = [];
        }
        if (!isset($field['options']['target_element'])) {
            $field['options']['target_element'] = $subSpec;
        }
        if (is_array($field['options']['target_element'])) {
            $field['options']['target_element']['options']['original-name'] = $name;
            $options = $field['options'];
            $options['original-name'] = $name;
            $elements[] = ['spec' => [
                'name' => $name,
                'type' => 'collection',
                'options' => $options
            ]];
        } else {
            $elements[] = $field;
        }
    }

    private function filterOut($input, $type = 'field') {
        switch($type) {
            case ('field'):
                return array_intersect_key($input,
                    [
                        'name' => true,
                        'type' => true,
                        'attributes' => true,
                        'options' => true,
                    ]
                );
                break;
            case('fieldSet'):
                return array_intersect_key($input,
                    [
                        'name' => true,
                        'type' => true,
                        'object' => true,
                        'attributes' => true,
                        'hydrator' => true,
                        'options' => true,
                    ]
                );
                break;
            case('inputFilter'):
                return array_intersect_key($input,
                    [
                        'name' => true,
                        'required' => true,
                        'error_message' => true,
                        'fallback_value' => true,
                        'break_on_failure' => true,
                        'filters' => true,
                        'validators' => true,
                        'allow_empty' => true,
                        'continue_if_empty' => true
                    ]
                );
                break;
            default:
                throw new \Exception('Invalid type "' . $type . '" given');
        }
    }
}